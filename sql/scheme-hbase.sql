CREATE SCHEMA RUOHBASE;
USE RUOHBASE;
-- ----------------------------
-- 1、部门表
-- ----------------------------
-- 创建 HBase 表
-- 创建 HBase 表 sys_dept
DROP TABLE IF EXISTS RUOHBASE.SYS_DEPT;
CREATE TABLE RUOHBASE.SYS_DEPT
(
    "dept_id"           INTEGER           NOT NULL  PRIMARY KEY,        -- 部门id
    "parent_id"         INTEGER           DEFAULT 0,                     -- 父部门id
    "ancestors"         VARCHAR          DEFAULT '',                    -- 祖级列表
    "dept_name"         VARCHAR          DEFAULT '',                    -- 部门名称
    "order_num"         INTEGER              DEFAULT 0,                     -- 显示顺序
    "leader"            VARCHAR          DEFAULT NULL,                  -- 负责人
    "phone"             VARCHAR          DEFAULT NULL,                  -- 联系电话
    "email"             VARCHAR          DEFAULT NULL,                  -- 邮箱
    "status"            VARCHAR(1)             DEFAULT '0',                   -- 部门状态（0正常 1停用）
    "del_flag"          VARCHAR(1)             DEFAULT '0',                   -- 删除标志（0代表存在 2代表删除）
    "create_by"         VARCHAR          DEFAULT '',                    -- 创建者
    "create_time"       DATE,                                         -- 创建时间
    "update_by"         VARCHAR          DEFAULT '',                    -- 更新者
    "update_time"       DATE                                         -- 更新时间
);
DROP SEQUENCE IF EXISTS RUOHBASE.SYS_DEPT_SEQUENCE;
CREATE SEQUENCE RUOHBASE.SYS_DEPT_SEQUENCE START WITH 1 increment BY 1;
DROP INDEX IF EXISTS sys_dept_idx_1 ON RUOHBASE.SYS_DEPT;
DROP INDEX IF EXISTS sys_dept_idx_2 ON RUOHBASE.SYS_DEPT;
DROP INDEX IF EXISTS sys_dept_idx_3 ON RUOHBASE.SYS_DEPT;
DROP INDEX IF EXISTS sys_dept_idx_4 ON RUOHBASE.SYS_DEPT;

CREATE INDEX sys_dept_idx_1 ON RUOHBASE.SYS_DEPT ("del_flag");
CREATE INDEX sys_dept_idx_2 ON RUOHBASE.SYS_DEPT ("parent_id");
CREATE INDEX sys_dept_idx_3 ON RUOHBASE.SYS_DEPT ("dept_name");
CREATE INDEX sys_dept_idx_4 ON RUOHBASE.SYS_DEPT ("status");

-- ----------------------------
-- 初始化-部门表数据
-- ----------------------------

UPSERT INTO RUOHBASE.SYS_DEPT("dept_id","parent_id","ancestors","dept_name","order_num","leader","phone","email","status","del_flag","create_by","create_time","update_by","update_time")
VALUES (100, 0, '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin',NOW() , '', NULL);

UPSERT INTO RUOHBASE.SYS_DEPT("dept_id","parent_id","ancestors","dept_name","order_num","leader","phone","email","status","del_flag","create_by","create_time","update_by","update_time")
VALUES(101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', NOW() , '', NULL);

UPSERT INTO RUOHBASE.SYS_DEPT("dept_id","parent_id","ancestors","dept_name","order_num","leader","phone","email","status","del_flag","create_by","create_time","update_by","update_time")
VALUES(102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', NOW() , '', NULL);

UPSERT INTO RUOHBASE.SYS_DEPT("dept_id","parent_id","ancestors","dept_name","order_num","leader","phone","email","status","del_flag","create_by","create_time","update_by","update_time")
VALUES(103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', NOW() , '', NULL);

UPSERT INTO RUOHBASE.SYS_DEPT("dept_id","parent_id","ancestors","dept_name","order_num","leader","phone","email","status","del_flag","create_by","create_time","update_by","update_time")
VALUES(104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', NOW() , '', NULL);

UPSERT INTO RUOHBASE.SYS_DEPT("dept_id","parent_id","ancestors","dept_name","order_num","leader","phone","email","status","del_flag","create_by","create_time","update_by","update_time")
VALUES(105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', NOW() , '', NULL);

UPSERT INTO RUOHBASE.SYS_DEPT("dept_id","parent_id","ancestors","dept_name","order_num","leader","phone","email","status","del_flag","create_by","create_time","update_by","update_time")
VALUES(106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', NOW() , '', NULL);

UPSERT INTO RUOHBASE.SYS_DEPT("dept_id","parent_id","ancestors","dept_name","order_num","leader","phone","email","status","del_flag","create_by","create_time","update_by","update_time")
VALUES(107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', NOW() , '', NULL);

UPSERT INTO RUOHBASE.SYS_DEPT("dept_id","parent_id","ancestors","dept_name","order_num","leader","phone","email","status","del_flag","create_by","create_time","update_by","update_time")
VALUES(108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', NOW() , '', NULL);

UPSERT INTO RUOHBASE.SYS_DEPT("dept_id","parent_id","ancestors","dept_name","order_num","leader","phone","email","status","del_flag","create_by","create_time","update_by","update_time")
VALUES(109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', NOW() , '', NULL);


-- ----------------------------
-- 2、用户信息表
-- ----------------------------
DROP TABLE IF EXISTS RUOHBASE.SYS_USER;
CREATE TABLE RUOHBASE.SYS_USER (
                                   "user_id"         INTEGER           NOT NULL PRIMARY KEY,      -- 用户ID
                                   "dept_id"         INTEGER           DEFAULT NULL,              -- 部门ID
                                   "user_name"       VARCHAR(30)       ,                  -- 用户账号
                                   "nick_name"       VARCHAR(30)       ,                  -- 用户昵称
                                   "user_type"       VARCHAR(2)        DEFAULT '00',              -- 用户类型（00系统用户）
                                   "email"           VARCHAR(50)       DEFAULT '',                -- 用户邮箱
                                   "phonenumber"     VARCHAR(11)       DEFAULT '',                -- 手机号码
                                   "sex"             VARCHAR(1)        DEFAULT '0',               -- 用户性别（0男 1女 2未知）
                                   "avatar"          VARCHAR(100)      DEFAULT '',                -- 头像地址
                                   "password"        VARCHAR(100)      DEFAULT '',                -- 密码
                                   "status"          VARCHAR(1)        DEFAULT '0',               -- 帐号状态（0正常 1停用）
                                   "del_flag"        VARCHAR(1)        DEFAULT '0',               -- 删除标志（0代表存在 2代表删除）
                                   "login_ip"        VARCHAR(128)      DEFAULT '',                -- 最后登录IP
                                   "login_date"      DATE,                                        -- 最后登录时间
                                   "create_by"       VARCHAR(64)       DEFAULT '',                -- 创建者
                                   "create_time"     DATE,                                        -- 创建时间
                                   "update_by"       VARCHAR(64)       DEFAULT '',                -- 更新者
                                   "update_time"     DATE,                                        -- 更新时间
                                   "remark"          VARCHAR(500)      DEFAULT NULL               -- 备注
);

DROP SEQUENCE IF EXISTS RUOHBASE.SYS_USER_SEQUENCE;
CREATE SEQUENCE RUOHBASE.SYS_USER_SEQUENCE START WITH 1 increment BY 1;
DROP INDEX IF EXISTS sys_user_idx_1 ON RUOHBASE.SYS_USER;
DROP INDEX IF EXISTS sys_user_idx_2 ON RUOHBASE.SYS_USER;
DROP INDEX IF EXISTS sys_user_idx_3 ON RUOHBASE.SYS_USER;
DROP INDEX IF EXISTS sys_user_idx_4 ON RUOHBASE.SYS_USER;
DROP INDEX IF EXISTS sys_user_idx_5 ON RUOHBASE.SYS_USER;
DROP INDEX IF EXISTS sys_user_idx_6 ON RUOHBASE.SYS_USER;

CREATE INDEX sys_user_idx_1 ON RUOHBASE.SYS_USER ("del_flag");
CREATE INDEX sys_user_idx_2 ON RUOHBASE.SYS_USER ("user_name");
CREATE INDEX sys_user_idx_3 ON RUOHBASE.SYS_USER ("status");
CREATE INDEX sys_user_idx_4 ON RUOHBASE.SYS_USER ("phonenumber");
CREATE INDEX sys_user_idx_5 ON RUOHBASE.SYS_USER ("create_time");
CREATE INDEX sys_user_idx_6 ON RUOHBASE.SYS_USER ("dept_id");

-- ----------------------------
-- 初始化-用户信息表数据
-- ----------------------------
UPSERT INTO RUOHBASE.SYS_USER("user_id", "dept_id", "user_name", "nick_name", "user_type", "email", "phonenumber", "sex", "avatar", "password", "status", "del_flag", "login_ip", "login_date", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1, 103, 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', CURRENT_DATE(), 'admin', CURRENT_DATE(), '', NULL, '管理员');

UPSERT INTO RUOHBASE.SYS_USER("user_id", "dept_id", "user_name", "nick_name", "user_type", "email", "phonenumber", "sex", "avatar", "password", "status", "del_flag", "login_ip", "login_date", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (2, 105, 'ry', '若依', '00', 'ry@qq.com', '15666666666', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', CURRENT_DATE(), 'admin', CURRENT_DATE(), '', NULL, '测试员');



-- ----------------------------
-- 3、岗位信息表
-- ----------------------------
DROP TABLE IF EXISTS RUOHBASE.SYS_POST;
CREATE TABLE RUOHBASE.SYS_POST (
                                   "post_id"       INTEGER           NOT NULL PRIMARY KEY,      -- 岗位ID
                                   "post_code"     VARCHAR(64)       ,                  -- 岗位编码
                                   "post_name"     VARCHAR(50)       ,                  -- 岗位名称
                                   "post_sort"     INTEGER           ,                  -- 显示顺序
                                   "status"        VARCHAR(1)        ,                  -- 状态（0正常 1停用）
                                   "create_by"     VARCHAR(64)       DEFAULT '',                -- 创建者
                                   "create_time"   DATE,                                       -- 创建时间
                                   "update_by"     VARCHAR(64)       DEFAULT '',                -- 更新者
                                   "update_time"   DATE,                                       -- 更新时间
                                   "remark"        VARCHAR(500)      DEFAULT NULL               -- 备注
);

DROP SEQUENCE IF EXISTS RUOHBASE.SYS_POST_SEQUENCE;
CREATE SEQUENCE RUOHBASE.SYS_POST_SEQUENCE START WITH 1 increment BY 1;
DROP INDEX IF EXISTS sys_post_idx_1 ON RUOHBASE.SYS_POST;
DROP INDEX IF EXISTS sys_post_idx_2 ON RUOHBASE.SYS_POST;
DROP INDEX IF EXISTS sys_post_idx_3 ON RUOHBASE.SYS_POST;

CREATE INDEX sys_post_idx_1 ON RUOHBASE.SYS_POST ("post_code");
CREATE INDEX sys_post_idx_2 ON RUOHBASE.SYS_POST ("status");
CREATE INDEX sys_post_idx_3 ON RUOHBASE.SYS_POST ("post_name");

-- ----------------------------
-- 初始化-岗位信息表数据
-- ----------------------------
UPSERT INTO RUOHBASE.SYS_POST("post_id", "post_code", "post_name", "post_sort", "status", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES(1, 'ceo', '董事长', 1, '0', 'admin', CURRENT_DATE(), '', NULL, '');

UPSERT INTO RUOHBASE.SYS_POST("post_id", "post_code", "post_name", "post_sort", "status", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES(2, 'se', '项目经理', 2, '0', 'admin', CURRENT_DATE(), '', NULL, '');

UPSERT INTO RUOHBASE.SYS_POST("post_id", "post_code", "post_name", "post_sort", "status", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES(3, 'hr', '人力资源', 3, '0', 'admin', CURRENT_DATE(), '', NULL, '');

UPSERT INTO RUOHBASE.SYS_POST("post_id", "post_code", "post_name", "post_sort", "status", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES(4, 'user', '普通员工', 4, '0', 'admin', CURRENT_DATE(), '', NULL, '');




-- ----------------------------
-- 4、角色信息表
-- ----------------------------
DROP TABLE IF EXISTS RUOHBASE.SYS_ROLE;
CREATE TABLE RUOHBASE.SYS_ROLE (
                                   "role_id"              INTEGER         NOT NULL  PRIMARY KEY,  -- 角色ID
                                   "role_name"            VARCHAR(30)     ,                             -- 角色名称
                                   "role_key"             VARCHAR(100)    ,                             -- 角色权限字符串
                                   "role_sort"            INTEGER         ,                             -- 显示顺序
                                   "data_scope"           VARCHAR(1)      DEFAULT '1',                          -- 数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）
                                   "menu_check_strictly"  BOOLEAN         DEFAULT true,                             -- 菜单树选择项是否关联显示
                                   "dept_check_strictly"  BOOLEAN         DEFAULT true,                             -- 部门树选择项是否关联显示
                                   "status"               VARCHAR(1)      ,                             -- 角色状态（0正常 1停用）
                                   "del_flag"             VARCHAR(1)      DEFAULT '0',                          -- 删除标志（0代表存在 2代表删除）
                                   "create_by"            VARCHAR(64)     DEFAULT '',                            -- 创建者
                                   "create_time"          DATE,                                                -- 创建时间
                                   "update_by"            VARCHAR(64)     DEFAULT '',                            -- 更新者
                                   "update_time"          DATE,                                                -- 更新时间
                                   "remark"               VARCHAR(500)    DEFAULT NULL                           -- 备注
);



DROP SEQUENCE IF EXISTS RUOHBASE.SYS_ROLE_SEQUENCE;
CREATE SEQUENCE RUOHBASE.SYS_ROLE_SEQUENCE START WITH 1 increment BY 1;
DROP INDEX IF EXISTS sys_role_idx_1 ON RUOHBASE.SYS_ROLE;
DROP INDEX IF EXISTS sys_role_idx_2 ON RUOHBASE.SYS_ROLE;
DROP INDEX IF EXISTS sys_role_idx_3 ON RUOHBASE.SYS_ROLE;
DROP INDEX IF EXISTS sys_role_idx_4 ON RUOHBASE.SYS_ROLE;
DROP INDEX IF EXISTS sys_role_idx_5 ON RUOHBASE.SYS_ROLE;

CREATE INDEX sys_role_idx_1 ON RUOHBASE.SYS_ROLE ("del_flag");
CREATE INDEX sys_role_idx_2 ON RUOHBASE.SYS_ROLE ("role_name");
CREATE INDEX sys_role_idx_3 ON RUOHBASE.SYS_ROLE ("status");
CREATE INDEX sys_role_idx_4 ON RUOHBASE.SYS_ROLE ("role_key");
CREATE INDEX sys_role_idx_5 ON RUOHBASE.SYS_ROLE ("create_time");


-- ----------------------------
-- 初始化-角色信息表数据
-- ----------------------------
UPSERT INTO RUOHBASE.SYS_ROLE("role_id","role_name","role_key","role_sort","data_scope","menu_check_strictly","dept_check_strictly","status","del_flag","create_by","create_time","update_by","update_time","remark")
VALUES (1, '超级管理员', 'admin', 1, '1', true, true, '0', '0', 'admin', NOW(), '', NULL, '超级管理员');

UPSERT INTO RUOHBASE.SYS_ROLE("role_id","role_name","role_key","role_sort","data_scope","menu_check_strictly","dept_check_strictly","status","del_flag","create_by","create_time","update_by","update_time","remark")
VALUES (2, '普通角色', 'common', 2, '1', true, true, '0', '0', 'admin', NOW(), '', NULL, '普通角色');

-- ----------------------------
-- 5、菜单权限表
-- ----------------------------
DROP TABLE IF EXISTS RUOHBASE.SYS_MENU;
CREATE TABLE RUOHBASE.SYS_MENU (
                                   "menu_id"           INTEGER           NOT NULL  PRIMARY KEY,        -- 菜单ID
                                   "menu_name"         VARCHAR(50)       ,                     -- 菜单名称
                                   "parent_id"         INTEGER           DEFAULT 0,                    -- 父菜单ID
                                   "order_num"         INTEGER           DEFAULT 0,                    -- 显示顺序
                                   "path"              VARCHAR(200)      DEFAULT '',                   -- 路由地址
                                   "component"         VARCHAR(255)      DEFAULT NULL,                 -- 组件路径
                                   "query"             VARCHAR(255)      DEFAULT NULL,                 -- 路由参数
                                   "is_frame"          VARCHAR           DEFAULT '1',                    -- 是否为外链（0是 1否）
                                   "is_cache"          VARCHAR           DEFAULT '0',                    -- 是否缓存（0缓存 1不缓存）
                                   "menu_type"         CHAR(1)           DEFAULT '',                   -- 菜单类型（M目录 C菜单 F按钮）
                                   "visible"           CHAR(1)           DEFAULT '0',                  -- 菜单状态（0显示 1隐藏）
                                   "status"            CHAR(1)           DEFAULT '0',                  -- 菜单状态（0正常 1停用）
                                   "perms"             VARCHAR(100)      DEFAULT NULL,                 -- 权限标识
                                   "icon"              VARCHAR(100)      DEFAULT '#',                  -- 菜单图标
                                   "create_by"         VARCHAR(64)       DEFAULT '',                   -- 创建者
                                   "create_time"       DATE,                                          -- 创建时间
                                   "update_by"         VARCHAR(64)       DEFAULT '',                   -- 更新者
                                   "update_time"       DATE,                                          -- 更新时间
                                   "remark"            VARCHAR(500)      DEFAULT ''                    -- 备注
);

DROP SEQUENCE IF EXISTS RUOHBASE.SYS_MENU_SEQUENCE;
CREATE SEQUENCE RUOHBASE.SYS_MENU_SEQUENCE START WITH 1 increment BY 1;
DROP INDEX IF EXISTS sys_menu_idx_1 ON RUOHBASE.SYS_MENU;
DROP INDEX IF EXISTS sys_menu_idx_2 ON RUOHBASE.SYS_MENU;
DROP INDEX IF EXISTS sys_menu_idx_3 ON RUOHBASE.SYS_MENU;

CREATE INDEX sys_menu_idx_1 ON RUOHBASE.SYS_MENU ("menu_name");
CREATE INDEX sys_menu_idx_2 ON RUOHBASE.SYS_MENU ("visible");
CREATE INDEX sys_menu_idx_3 ON RUOHBASE.SYS_MENU ("status");

-- ----------------------------
-- 初始化-菜单信息表数据
-- ----------------------------
-- 一级菜单
UPSERT INTO RUOHBASE.SYS_MENU("menu_id","menu_name","parent_id","order_num","path","component","query","is_frame","is_cache","menu_type","visible","status","perms","icon","create_by","create_time","update_by","update_time","remark")
VALUES (1, '系统管理', 0, 1, 'system', NULL, '', '1', '0', 'M', '0', '0', '', 'system', 'admin', NOW(), '', NULL, '系统管理目录');
UPSERT INTO RUOHBASE.SYS_MENU("menu_id","menu_name","parent_id","order_num","path","component","query","is_frame","is_cache","menu_type","visible","status","perms","icon","create_by","create_time","update_by","update_time","remark")
VALUES (2, '系统监控', 0, 2, 'monitor', NULL, '', '1', '0', 'M', '0', '0', '', 'monitor', 'admin', NOW(), '', NULL, '系统监控目录');
UPSERT INTO RUOHBASE.SYS_MENU("menu_id","menu_name","parent_id","order_num","path","component","query","is_frame","is_cache","menu_type","visible","status","perms","icon","create_by","create_time","update_by","update_time","remark")
VALUES (3, '系统工具', 0, 3, 'tool', NULL, '', '1', '0', 'M', '0', '0', '', 'tool', 'admin', NOW(), '', NULL, '系统工具目录');
UPSERT INTO RUOHBASE.SYS_MENU("menu_id","menu_name","parent_id","order_num","path","component","query","is_frame","is_cache","menu_type","visible","status","perms","icon","create_by","create_time","update_by","update_time","remark")
VALUES (4, '若依官网', 0, 4, 'http://ruoyi.vip', NULL, '', '0', '0', 'M', '0', '0', '', 'guide', 'admin', NOW(), '', NULL, '若依官网地址');
-- 二级菜单
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', '', '1', '0', 'C', '0', '0', 'system:user:list', 'user', 'admin', NOW(), '', NULL, '用户管理菜单');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', '', '1', '0', 'C', '0', '0', 'system:role:list', 'peoples', 'admin', NOW(), '', NULL, '角色管理菜单');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', '', '1', '0', 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', NOW(), '', NULL, '菜单管理菜单');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', '', '1', '0', 'C', '0', '0', 'system:dept:list', 'tree', 'admin', NOW(), '', NULL, '部门管理菜单');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', '', '1', '0', 'C', '0', '0', 'system:post:list', 'post', 'admin', NOW(), '', NULL, '岗位管理菜单');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', '', '1', '0', 'C', '0', '0', 'system:dict:list', 'dict', 'admin', NOW(), '', NULL, '字典管理菜单');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', '', '1', '0', 'C', '0', '0', 'system:config:list', 'edit', 'admin', NOW(), '', NULL, '参数设置菜单');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', '', '1', '0', 'C', '0', '0', 'system:notice:list', 'message', 'admin', NOW(), '', NULL, '通知公告菜单');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (108, '日志管理', 1, 9, 'log', '', '', '1', '0', 'M', '0', '0', '', 'log', 'admin', NOW(), '', NULL, '日志管理菜单');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', '', '1', '0', 'C', '0', '0', 'monitor:online:list', 'online', 'admin', NOW(), '', NULL, '在线用户菜单');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', '', '1', '0', 'C', '0', '0', 'monitor:job:list', 'job', 'admin', NOW(), '', NULL, '定时任务菜单');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (111, 'Sentinel控制台', 2, 3, 'http://localhost:8718', '', '', '0', '0', 'C', '0', '0', 'monitor:sentinel:list', 'sentinel', 'admin', NOW(), '', NULL, '流量控制菜单');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (112, 'Nacos控制台', 2, 4, 'http://localhost:8848/nacos', '', '', '0', '0', 'C', '0', '0', 'monitor:nacos:list', 'nacos', 'admin', NOW(), '', NULL, '服务治理菜单');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (113, 'Admin控制台', 2, 5, 'http://localhost:9100/login', '', '', '0', '0', 'C', '0', '0', 'monitor:server:list', 'server', 'admin', NOW(), '', NULL, '服务监控菜单');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (114, '表单构建', 3, 1, 'build', 'tool/build/index', '', '1', '0', 'C', '0', '0', 'tool:build:list', 'build', 'admin', NOW(), '', NULL, '表单构建菜单');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (115, '代码生成', 3, 2, 'gen', 'tool/gen/index', '', '1', '0', 'C', '0', '0', 'tool:gen:list', 'code', 'admin', NOW(), '', NULL, '代码生成菜单');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (116, '系统接口', 3, 3, 'http://localhost:8080/swagger-ui/index.html', '', '', '0', '0', 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', NOW(), '', NULL, '系统接口菜单');
-- 三级菜单
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (500, '操作日志', 108, 1, 'operlog', 'system/operlog/index', '', '1', '0', 'C', '0', '0', 'system:operlog:list', 'form', 'admin', NOW(), '', NULL, '操作日志菜单');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (501, '登录日志', 108, 2, 'logininfor', 'system/logininfor/index', '', '1', '0', 'C', '0', '0', 'system:logininfor:list', 'logininfor', 'admin', NOW(), '', NULL, '登录日志菜单');
-- 用户管理按钮
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1000, '用户查询', 100, 1, '', '', '', '1', '0', 'F', '0', '0', 'system:user:query', '#', 'admin', NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1001, '用户新增', 100, 2, '', '', '', '1', '0', 'F', '0', '0', 'system:user:add', '#', 'admin', NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1002, '用户修改', 100, 3, '', '', '', '1', '0', 'F', '0', '0', 'system:user:edit', '#', 'admin', NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1003, '用户删除', 100, 4, '', '', '', '1', '0', 'F', '0', '0', 'system:user:remove', '#', 'admin', NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1004, '用户导出', 100, 5, '', '', '', '1', '0', 'F', '0', '0', 'system:user:export', '#', 'admin', NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1005, '用户导入', 100, 6, '', '', '', '1', '0', 'F', '0', '0', 'system:user:import', '#', 'admin', NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1006, '重置密码', 100, 7, '', '', '', '1', '0', 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', NOW(), '', NULL, '');
-- 角色管理按钮
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1007, '角色查询', 101, 1, '', '', '', '1', '0', 'F', '0', '0', 'system:role:query', '#', 'admin', NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1008, '角色新增', 101, 2, '', '', '', '1', '0', 'F', '0', '0', 'system:role:add', '#', 'admin', NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1009, '角色修改', 101, 3, '', '', '', '1', '0', 'F', '0', '0', 'system:role:edit', '#', 'admin', NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1010, '角色删除', 101, 4, '', '', '', '1', '0', 'F', '0', '0', 'system:role:remove', '#', 'admin', NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1011, '角色导出', 101, 5, '', '', '', '1', '0', 'F', '0', '0', 'system:role:export', '#', 'admin', NOW(), '', NULL, '');
-- 菜单管理按钮
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1012, '菜单查询', 102, 1, '', '', '', '1', '0', 'F', '0', '0', 'system:menu:query', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1013, '菜单新增', 102, 2, '', '', '', '1', '0', 'F', '0', '0', 'system:menu:add', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1014, '菜单修改', 102, 3, '', '', '', '1', '0', 'F', '0', '0', 'system:menu:edit', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1015, '菜单删除', 102, 4, '', '', '', '1', '0', 'F', '0', '0', 'system:menu:remove', '#', 'admin',  NOW(), '', NULL, '');
-- 部门管理按钮
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1016, '部门查询', 103, 1, '', '', '', '1', '0', 'F', '0', '0', 'system:dept:query', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1017, '部门新增', 103, 2, '', '', '', '1', '0', 'F', '0', '0', 'system:dept:add', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1018, '部门修改', 103, 3, '', '', '', '1', '0', 'F', '0', '0', 'system:dept:edit', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1019, '部门删除', 103, 4, '', '', '', '1', '0', 'F', '0', '0', 'system:dept:remove', '#', 'admin',  NOW(), '', NULL, '');
-- 岗位管理按钮
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1020, '岗位查询', 104, 1, '', '', '', '1', '0', 'F', '0', '0', 'system:post:query', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1021, '岗位新增', 104, 2, '', '', '', '1', '0', 'F', '0', '0', 'system:post:add', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1022, '岗位修改', 104, 3, '', '', '', '1', '0', 'F', '0', '0', 'system:post:edit', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1023, '岗位删除', 104, 4, '', '', '', '1', '0', 'F', '0', '0', 'system:post:remove', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1024, '岗位导出', 104, 5, '', '', '', '1', '0', 'F', '0', '0', 'system:post:export', '#', 'admin',  NOW(), '', NULL, '');
-- 字典管理按钮
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1025, '字典查询', 105, 1, '#', '', '', '1', '0', 'F', '0', '0', 'system:dict:query', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1026, '字典新增', 105, 2, '#', '', '', '1', '0', 'F', '0', '0', 'system:dict:add', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1027, '字典修改', 105, 3, '#', '', '', '1', '0', 'F', '0', '0', 'system:dict:edit', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1028, '字典删除', 105, 4, '#', '', '', '1', '0', 'F', '0', '0', 'system:dict:remove', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1029, '字典导出', 105, 5, '#', '', '', '1', '0', 'F', '0', '0', 'system:dict:export', '#', 'admin',  NOW(), '', NULL, '');
-- 参数设置按钮
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1030, '参数查询', 106, 1, '#', '', '', '1', '0', 'F', '0', '0', 'system:config:query', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1031, '参数新增', 106, 2, '#', '', '', '1', '0', 'F', '0', '0', 'system:config:add', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1032, '参数修改', 106, 3, '#', '', '', '1', '0', 'F', '0', '0', 'system:config:edit', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1033, '参数删除', 106, 4, '#', '', '', '1', '0', 'F', '0', '0', 'system:config:remove', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1034, '参数导出', 106, 5, '#', '', '', '1', '0', 'F', '0', '0', 'system:config:export', '#', 'admin',  NOW(), '', NULL, '');
-- 通知公告按钮
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1035, '公告查询', 107, 1, '#', '', '', '1', '0', 'F', '0', '0', 'system:notice:query', '#', 'admin',  NOW(), '', NULL, '');
 UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1035, '公告查询', 107, 1, '#', '', '', '1', '0', 'F', '0', '0', 'system:notice:query', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1036, '公告新增', 107, 2, '#', '', '', '1', '0', 'F', '0', '0', 'system:notice:add', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1037, '公告修改', 107, 3, '#', '', '', '1', '0', 'F', '0', '0', 'system:notice:edit', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1038, '公告删除', 107, 4, '#', '', '', '1', '0', 'F', '0', '0', 'system:notice:remove', '#', 'admin',  NOW(), '', NULL, '');
-- 操作日志按钮
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1039, '操作查询', 500, 1, '#', '', '', '1', '0', 'F', '0', '0', 'system:operlog:query', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1040, '操作删除', 500, 2, '#', '', '', '1', '0', 'F', '0', '0', 'system:operlog:remove', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1041, '日志导出', 500, 3, '#', '', '', '1', '0', 'F', '0', '0', 'system:operlog:export', '#', 'admin',  NOW(), '', NULL, '');
-- 登录日志按钮
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1042, '登录查询', 501, 1, '#', '', '', '1', '0', 'F', '0', '0', 'system:logininfor:query', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1043, '登录删除', 501, 2, '#', '', '', '1', '0', 'F', '0', '0', 'system:logininfor:remove', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1044, '日志导出', 501, 3, '#', '', '', '1', '0', 'F', '0', '0', 'system:logininfor:export', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1045, '账户解锁', 501, 4, '#', '', '', '1', '0', 'F', '0', '0', 'system:logininfor:unlock', '#', 'admin',  NOW(), '', NULL, '');
-- 在线用户按钮
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1046, '在线查询', 109, 1, '#', '', '', '1', '0', 'F', '0', '0', 'monitor:online:query', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1047, '批量强退', 109, 2, '#', '', '', '1', '0', 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1048, '单条强退', 109, 3, '#', '', '', '1', '0', 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin',  NOW(), '', NULL, '');
-- 定时任务按钮
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1049, '任务查询', 110, 1, '#', '', '', '1', '0', 'F', '0', '0', 'monitor:job:query', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1050, '任务新增', 110, 2, '#', '', '', '1', '0', 'F', '0', '0', 'monitor:job:add', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1051, '任务修改', 110, 3, '#', '', '', '1', '0', 'F', '0', '0', 'monitor:job:edit', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1052, '任务删除', 110, 4, '#', '', '', '1', '0', 'F', '0', '0', 'monitor:job:remove', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1053, '状态修改', 110, 5, '#', '', '', '1', '0', 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1054, '任务导出', 110, 6, '#', '', '', '1', '0', 'F', '0', '0', 'monitor:job:export', '#', 'admin',  NOW(), '', NULL, '');
-- 代码生成按钮
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1055, '生成查询', 115, 1, '#', '', '', '1', '0', 'F', '0', '0', 'tool:gen:query', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1056, '生成修改', 115, 2, '#', '', '', '1', '0', 'F', '0', '0', 'tool:gen:edit', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1057, '生成删除', 115, 3, '#', '', '', '1', '0', 'F', '0', '0', 'tool:gen:remove', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1058, '导入代码', 115, 2, '#', '', '', '1', '0', 'F', '0', '0', 'tool:gen:import', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1059, '预览代码', 115, 4, '#', '', '', '1', '0', 'F', '0', '0', 'tool:gen:preview', '#', 'admin',  NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_MENU ("menu_id", "menu_name", "parent_id", "order_num", "path", "component", "query", "is_frame", "is_cache", "menu_type", "visible", "status", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1060, '生成代码', 115, 5, '#', '', '', '1', '0', 'F', '0', '0', 'tool:gen:code', '#', 'admin',  NOW(), '', NULL, '');


-- ----------------------------
-- 6、用户和角色关联表  用户N-1角色
-- ----------------------------
DROP TABLE IF EXISTS RUOHBASE.SYS_USER_ROLE;
CREATE TABLE RUOHBASE.SYS_USER_ROLE
(
    "id"        INTEGER       NOT NULL  PRIMARY KEY,        -- id
    "user_id"   INTEGER       ,       -- 用户ID
    "role_id"   INTEGER              -- 角色ID
);

DROP SEQUENCE IF EXISTS RUOHBASE.SYS_USER_ROLE_SEQUENCE;
CREATE SEQUENCE RUOHBASE.SYS_USER_ROLE_SEQUENCE START WITH 1 increment BY 1;
DROP INDEX IF EXISTS sys_user_role_idx_1 ON RUOHBASE.SYS_USER_ROLE;
DROP INDEX IF EXISTS sys_user_role_idx_2 ON RUOHBASE.SYS_USER_ROLE;

CREATE INDEX sys_user_role_idx_1 ON RUOHBASE.SYS_USER_ROLE ("user_id");
CREATE INDEX sys_user_role_idx_2 ON RUOHBASE.SYS_USER_ROLE ("role_id");


-- ----------------------------
-- 初始化-用户和角色关联表数据
-- ----------------------------
UPSERT INTO RUOHBASE.SYS_USER_ROLE("id","user_id", "role_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_USER_ROLE_SEQUENCE, 1, 1);
UPSERT INTO RUOHBASE.SYS_USER_ROLE("id","user_id", "role_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_USER_ROLE_SEQUENCE, 2, 2);

-- ----------------------------
-- 7、角色和菜单关联表  角色1-N菜单
-- ----------------------------
DROP TABLE IF EXISTS RUOHBASE.SYS_ROLE_MENU;
CREATE TABLE RUOHBASE.SYS_ROLE_MENU (
                                        "id"                INTEGER       NOT NULL  PRIMARY KEY,        -- id
                                        "role_id"           INTEGER,                                    -- 角色ID
                                        "menu_id"           INTEGER                                     -- 菜单ID
);

DROP SEQUENCE IF EXISTS RUOHBASE.SYS_ROLE_MENU_SEQUENCE;
CREATE SEQUENCE RUOHBASE.SYS_ROLE_MENU_SEQUENCE START WITH 1 increment BY 1;
DROP INDEX IF EXISTS sys_role_menu_idx_1 ON RUOHBASE.SYS_ROLE_MENU;
DROP INDEX IF EXISTS sys_role_menu_idx_2 ON RUOHBASE.SYS_ROLE_MENU;

CREATE INDEX sys_role_menu_idx_1 ON RUOHBASE.SYS_ROLE_MENU ("role_id");
CREATE INDEX sys_role_menu_idx_2 ON RUOHBASE.SYS_ROLE_MENU ("menu_id");
-- ----------------------------
-- 初始化-角色和菜单关联表数据
-- ----------------------------
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 2);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 3);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 4);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 100);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 101);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 102);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 103);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 104);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 105);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 106);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 107);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 108);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 109);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 110);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 111);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 112);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 113);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 114);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 115);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 116);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 500);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 501);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1000);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1001);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1002);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1003);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1004);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1005);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1006);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1007);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1008);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1009);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1010);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1011);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1012);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1013);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1014);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1015);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1016);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1017);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1018);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1019);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1020);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1021);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1022);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1023);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1024);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1025);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1026);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1027);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1028);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1029);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1030);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1031);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1032);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1033);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1034);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1035);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1036);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1037);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1038);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1039);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1040);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1041);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1042);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1043);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1044);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1045);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1046);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1047);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1048);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1049);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1050);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1051);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1052);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1053);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1054);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1055);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1056);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1057);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1058);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1059);
UPSERT INTO RUOHBASE.SYS_ROLE_MENU("id","role_id", "menu_id") VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_MENU_SEQUENCE, 2, 1060);



-- ----------------------------
-- 8、角色和部门关联表  角色1-N部门
-- ----------------------------
DROP TABLE IF EXISTS RUOHBASE.SYS_ROLE_DEPT;
CREATE TABLE RUOHBASE.SYS_ROLE_DEPT (
                                        "id"        INTEGER           NOT NULL  PRIMARY KEY,        -- id
                                        "role_id"   INTEGER           ,                     -- 角色ID
                                        "dept_id"   INTEGER                               -- 部门ID
);

DROP SEQUENCE IF EXISTS RUOHBASE.SYS_ROLE_DEPT_SEQUENCE;
CREATE SEQUENCE RUOHBASE.SYS_ROLE_DEPT_SEQUENCE START WITH 1 increment BY 1;
DROP INDEX IF EXISTS sys_role_dept_idx_1 ON RUOHBASE.SYS_ROLE_DEPT;
DROP INDEX IF EXISTS sys_role_dept_idx_2 ON RUOHBASE.SYS_ROLE_DEPT;

CREATE INDEX sys_role_dept_idx_1 ON RUOHBASE.SYS_ROLE_DEPT ("role_id");
CREATE INDEX sys_role_dept_idx_2 ON RUOHBASE.SYS_ROLE_DEPT ("dept_id");
-- ----------------------------
-- 初始化-角色和部门关联表数据
-- ----------------------------
UPSERT INTO RUOHBASE.SYS_ROLE_DEPT("id","role_id", "dept_id")
VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_DEPT_SEQUENCE,2, 100);

UPSERT INTO RUOHBASE.SYS_ROLE_DEPT("id","role_id", "dept_id")
VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_DEPT_SEQUENCE,2, 101);

UPSERT INTO RUOHBASE.SYS_ROLE_DEPT("id","role_id", "dept_id")
VALUES (NEXT VALUE FOR RUOHBASE.SYS_ROLE_DEPT_SEQUENCE,2, 105);

-- ----------------------------
-- 9、用户与岗位关联表  用户1-N岗位
-- ----------------------------
DROP TABLE IF EXISTS RUOHBASE.SYS_USER_POST;
CREATE TABLE RUOHBASE.SYS_USER_POST (
                                        "id"        INTEGER           NOT NULL  PRIMARY KEY,        -- id
                                        "user_id"   INTEGER           ,          -- 用户ID
                                        "post_id"   INTEGER                     -- 岗位ID
);


DROP SEQUENCE IF EXISTS RUOHBASE.SYS_USER_POST_SEQUENCE;
CREATE SEQUENCE RUOHBASE.SYS_USER_POST_SEQUENCE START WITH 1 increment BY 1;
DROP INDEX IF EXISTS sys_user_post_idx_1 ON RUOHBASE.SYS_USER_POST;
DROP INDEX IF EXISTS sys_user_post_idx_2 ON RUOHBASE.SYS_USER_POST;

CREATE INDEX sys_user_post_idx_1 ON RUOHBASE.SYS_USER_POST ("user_id");
CREATE INDEX sys_user_post_idx_2 ON RUOHBASE.SYS_USER_POST ("post_id");

-- ----------------------------
-- 初始化-用户与岗位关联表数据
-- ----------------------------
UPSERT INTO RUOHBASE.SYS_USER_POST("id","user_id", "post_id")
VALUES (NEXT VALUE FOR RUOHBASE.SYS_USER_POST_SEQUENCE, 1, 1);

UPSERT INTO RUOHBASE.SYS_USER_POST("id","user_id", "post_id")
VALUES (NEXT VALUE FOR RUOHBASE.SYS_USER_POST_SEQUENCE, 2, 2);


-- ----------------------------
-- 10、操作日志记录
-- ----------------------------
DROP TABLE IF EXISTS RUOHBASE.SYS_OPER_LOG;
CREATE TABLE RUOHBASE.SYS_OPER_LOG (
                                       "oper_id"           INTEGER           NOT NULL  PRIMARY KEY,        -- 日志主键
                                       "title"             VARCHAR(50)       DEFAULT '',                   -- 模块标题
                                       "business_type"     INTEGER           DEFAULT 0,                    -- 业务类型（0其它 1新增 2修改 3删除）
                                       "method"            VARCHAR(100)      DEFAULT '',                   -- 方法名称
                                       "request_method"    VARCHAR(10)       DEFAULT '',                   -- 请求方式
                                       "operator_type"     INTEGER           DEFAULT 0,                    -- 操作类别（0其它 1后台用户 2手机端用户）
                                       "oper_name"         VARCHAR(50)       DEFAULT '',                   -- 操作人员
                                       "dept_name"         VARCHAR(50)       DEFAULT '',                   -- 部门名称
                                       "oper_url"          VARCHAR(255)      DEFAULT '',                   -- 请求URL
                                       "oper_ip"           VARCHAR(128)      DEFAULT '',                   -- 主机地址
                                       "oper_location"     VARCHAR(255)      DEFAULT '',                   -- 操作地点
                                       "oper_param"        VARCHAR(2000)     DEFAULT '',                   -- 请求参数
                                       "json_result"       VARCHAR(2000)     DEFAULT '',                   -- 返回参数
                                       "status"            INTEGER           DEFAULT 0,                    -- 操作状态（0正常 1异常）
                                       "error_msg"         VARCHAR(2000)     DEFAULT '',                   -- 错误消息
                                       "oper_time"         DATE,                                          -- 操作时间
                                       "cost_time"         INTEGER           DEFAULT 0                     -- 消耗时间
);


DROP SEQUENCE IF EXISTS RUOHBASE.SYS_OPER_LOG_SEQUENCE;
CREATE SEQUENCE RUOHBASE.SYS_OPER_LOG_SEQUENCE START WITH 1 increment BY 1;
DROP INDEX IF EXISTS sys_oper_log_idx_1 ON RUOHBASE.SYS_OPER_LOG;
DROP INDEX IF EXISTS sys_oper_log_idx_2 ON RUOHBASE.SYS_OPER_LOG;
DROP INDEX IF EXISTS sys_oper_log_idx_3 ON RUOHBASE.SYS_OPER_LOG;
DROP INDEX IF EXISTS sys_oper_log_idx_4 ON RUOHBASE.SYS_OPER_LOG;
DROP INDEX IF EXISTS sys_oper_log_idx_5 ON RUOHBASE.SYS_OPER_LOG;

CREATE INDEX sys_oper_log_idx_1 ON RUOHBASE.SYS_OPER_LOG ("title");
CREATE INDEX sys_oper_log_idx_2 ON RUOHBASE.SYS_OPER_LOG ("business_type");
CREATE INDEX sys_oper_log_idx_3 ON RUOHBASE.SYS_OPER_LOG ("status");
CREATE INDEX sys_oper_log_idx_4 ON RUOHBASE.SYS_OPER_LOG ("oper_name");
CREATE INDEX sys_oper_log_idx_5 ON RUOHBASE.SYS_OPER_LOG ("oper_time");


-- ----------------------------
-- 11、字典类型表
-- ----------------------------
DROP TABLE IF EXISTS RUOHBASE.SYS_DICT_TYPE;
CREATE TABLE RUOHBASE.SYS_DICT_TYPE (
                                        "dict_id"          INTEGER           NOT NULL  PRIMARY KEY,        -- 字典主键
                                        "dict_name"        VARCHAR(100)      DEFAULT '',                   -- 字典名称
                                        "dict_type"        VARCHAR(100)      DEFAULT '',                   -- 字典类型
                                        "status"           CHAR(1)           DEFAULT '0',                  -- 状态（0正常 1停用）
                                        "create_by"        VARCHAR(64)       DEFAULT '',                   -- 创建者
                                        "create_time"      DATE,                                          -- 创建时间
                                        "update_by"        VARCHAR(64)       DEFAULT '',                   -- 更新者
                                        "update_time"      DATE,                                          -- 更新时间
                                        "remark"           VARCHAR(500)      DEFAULT NULL                  -- 备注
);

DROP SEQUENCE IF EXISTS RUOHBASE.SYS_DICT_TYPE_SEQUENCE;
CREATE SEQUENCE RUOHBASE.SYS_DICT_TYPE_SEQUENCE START WITH 1 increment BY 1;
DROP INDEX IF EXISTS sys_dict_type_idx_1 ON RUOHBASE.SYS_DICT_TYPE;
DROP INDEX IF EXISTS sys_dict_type_idx_2 ON RUOHBASE.SYS_DICT_TYPE;
DROP INDEX IF EXISTS sys_dict_type_idx_3 ON RUOHBASE.SYS_DICT_TYPE;
DROP INDEX IF EXISTS sys_dict_type_idx_4 ON RUOHBASE.SYS_DICT_TYPE;

CREATE INDEX sys_dict_type_idx_1 ON RUOHBASE.SYS_DICT_TYPE ("dict_name");
CREATE INDEX sys_dict_type_idx_2 ON RUOHBASE.SYS_DICT_TYPE ("status");
CREATE INDEX sys_dict_type_idx_3 ON RUOHBASE.SYS_DICT_TYPE ("dict_type");
CREATE INDEX sys_dict_type_idx_4 ON RUOHBASE.SYS_DICT_TYPE ("create_time");


UPSERT INTO RUOHBASE.SYS_DICT_TYPE("dict_id", "dict_name", "dict_type", "status", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', NOW(), '', NULL, '用户性别列表');
UPSERT INTO RUOHBASE.SYS_DICT_TYPE("dict_id", "dict_name", "dict_type", "status", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', NOW(), '', NULL, '菜单状态列表');
UPSERT INTO RUOHBASE.SYS_DICT_TYPE("dict_id", "dict_name", "dict_type", "status", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', NOW(), '', NULL, '系统开关列表');
UPSERT INTO RUOHBASE.SYS_DICT_TYPE("dict_id", "dict_name", "dict_type", "status", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', NOW(), '', NULL, '任务状态列表');
UPSERT INTO RUOHBASE.SYS_DICT_TYPE("dict_id", "dict_name", "dict_type", "status", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', NOW(), '', NULL, '任务分组列表');
UPSERT INTO RUOHBASE.SYS_DICT_TYPE("dict_id", "dict_name", "dict_type", "status", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', NOW(), '', NULL, '系统是否列表');
UPSERT INTO RUOHBASE.SYS_DICT_TYPE("dict_id", "dict_name", "dict_type", "status", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', NOW(), '', NULL, '通知类型列表');
UPSERT INTO RUOHBASE.SYS_DICT_TYPE("dict_id", "dict_name", "dict_type", "status", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', NOW(), '', NULL, '通知状态列表');
UPSERT INTO RUOHBASE.SYS_DICT_TYPE("dict_id", "dict_name", "dict_type", "status", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', NOW(), '', NULL, '操作类型列表');
UPSERT INTO RUOHBASE.SYS_DICT_TYPE("dict_id", "dict_name", "dict_type", "status", "create_by", "create_time", "update_by", "update_time", "remark")
VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', NOW(), '', NULL, '登录状态列表');


-- ----------------------------
-- 12、字典数据表
-- ----------------------------
DROP TABLE IF EXISTS RUOHBASE.SYS_DICT_DATA;
CREATE TABLE RUOHBASE.SYS_DICT_DATA (
                                        "dict_code"        INTEGER           NOT NULL    PRIMARY KEY,        -- 字典编码
                                        "dict_sort"        INTEGER           DEFAULT 0,                     -- 字典排序
                                        "dict_label"       VARCHAR(100)      DEFAULT '',                    -- 字典标签
                                        "dict_value"       VARCHAR(100)      DEFAULT '',                    -- 字典键值
                                        "dict_type"        VARCHAR(100)      DEFAULT '',                    -- 字典类型
                                        "css_class"        VARCHAR(100)      DEFAULT NULL,                  -- 样式属性（其他样式扩展）
                                        "list_class"       VARCHAR(100)      DEFAULT NULL,                  -- 表格回显样式
                                        "is_default"       CHAR(1)           DEFAULT 'N',                   -- 是否默认（Y是 N否）
                                        "status"           CHAR(1)           DEFAULT '0',                   -- 状态（0正常 1停用）
                                        "create_by"        VARCHAR(64)       DEFAULT '',                    -- 创建者
                                        "create_time"      DATE,                                             -- 创建时间
                                        "update_by"        VARCHAR(64)       DEFAULT '',                    -- 更新者
                                        "update_time"      DATE,                                             -- 更新时间
                                        "remark"           VARCHAR(500)      DEFAULT NULL                   -- 备注
);

DROP SEQUENCE IF EXISTS RUOHBASE.SYS_DICT_DATA_SEQUENCE;
CREATE SEQUENCE RUOHBASE.SYS_DICT_DATA_SEQUENCE START WITH 1 increment BY 1;
DROP INDEX IF EXISTS sys_dict_data_idx_1 ON RUOHBASE.SYS_DICT_DATA;
DROP INDEX IF EXISTS sys_dict_data_idx_2 ON RUOHBASE.SYS_DICT_DATA;
DROP INDEX IF EXISTS sys_dict_data_idx_3 ON RUOHBASE.SYS_DICT_DATA;

CREATE INDEX sys_dict_data_idx_1 ON RUOHBASE.SYS_DICT_DATA ("dict_type");
CREATE INDEX sys_dict_data_idx_2 ON RUOHBASE.SYS_DICT_DATA ("dict_label");
CREATE INDEX sys_dict_data_idx_3 ON RUOHBASE.SYS_DICT_DATA ("status");


UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', NOW(), '', NULL, '性别男');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', NOW(), '', NULL, '性别女');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', NOW(), '', NULL, '性别未知');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', NOW(), '', NULL, '显示菜单');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', NOW(), '', NULL, '隐藏菜单');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', NOW(), '', NULL, '正常状态');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', NOW(), '', NULL, '停用状态');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', NOW(), '', NULL, '正常状态');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', NOW(), '', NULL, '停用状态');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', NOW(), '', NULL, '默认分组');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', NOW(), '', NULL, '系统分组');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', NOW(), '', NULL, '系统默认是');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', NOW(), '', NULL, '系统默认否');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', NOW(), '', NULL, '通知');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', NOW(), '', NULL, '公告');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', NOW(), '', NULL, '正常状态');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin',  NOW(), '', NULL, '关闭状态');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (18, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin',  NOW(), '', NULL, '其他操作');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (19, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin',  NOW(), '', NULL, '新增操作');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (20, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin',  NOW(), '', NULL, '修改操作');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (21, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin',  NOW(), '', NULL, '删除操作');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (22, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin',  NOW(), '', NULL, '授权操作');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (23, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin',  NOW(), '', NULL, '导出操作');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (24, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin',  NOW(), '', NULL, '导入操作');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (25, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin',  NOW(), '', NULL, '强退操作');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (26, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin',  NOW(), '', NULL, '生成操作');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (27, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin',  NOW(), '', NULL, '清空操作');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (28, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin',  NOW(), '', NULL, '正常状态');
UPSERT INTO RUOHBASE.SYS_DICT_DATA("dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status","create_by","create_time","update_by","update_time","remark")
VALUES (29, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin',  NOW(), '', NULL, '停用状态');


-- ----------------------------
-- 13、参数配置表
-- ----------------------------
DROP TABLE IF EXISTS RUOHBASE.SYS_CONFIG;
CREATE TABLE RUOHBASE.SYS_CONFIG (
                                     "config_id"         INTEGER           NOT NULL  PRIMARY KEY ,  -- 参数主键
                                     "config_name"       VARCHAR(100)      DEFAULT '',                             -- 参数名称
                                     "config_key"        VARCHAR(100)      DEFAULT '',                             -- 参数键名
                                     "config_value"      VARCHAR(500)      DEFAULT '',                             -- 参数键值
                                     "config_type"       CHAR(1)           DEFAULT 'N',                            -- 系统内置（Y是 N否）
                                     "create_by"         VARCHAR(64)       DEFAULT '',                             -- 创建者
                                     "create_time"       DATE,                                                    -- 创建时间
                                     "update_by"         VARCHAR(64)       DEFAULT '',                             -- 更新者
                                     "update_time"       DATE,                                                    -- 更新时间
                                     "remark"            VARCHAR(500)      DEFAULT NULL                             -- 备注
);




DROP SEQUENCE IF EXISTS RUOHBASE.SYS_CONFIG_SEQUENCE;
CREATE SEQUENCE RUOHBASE.SYS_CONFIG_SEQUENCE START WITH 1 increment BY 1;
DROP INDEX IF EXISTS sys_config_idx_1 ON RUOHBASE.SYS_CONFIG;
DROP INDEX IF EXISTS sys_config_idx_2 ON RUOHBASE.SYS_CONFIG;
DROP INDEX IF EXISTS sys_config_idx_3 ON RUOHBASE.SYS_CONFIG;
DROP INDEX IF EXISTS sys_config_idx_4 ON RUOHBASE.SYS_CONFIG;

CREATE INDEX sys_config_idx_1 ON RUOHBASE.SYS_CONFIG ("config_name");
CREATE INDEX sys_config_idx_2 ON RUOHBASE.SYS_CONFIG ("config_type");
CREATE INDEX sys_config_idx_3 ON RUOHBASE.SYS_CONFIG ("config_key");
CREATE INDEX sys_config_idx_4 ON RUOHBASE.SYS_CONFIG ("create_time");





UPSERT INTO RUOHBASE.SYS_CONFIG("config_id","config_name","config_key","config_value","config_type","create_by","create_time","update_by","update_time","remark")
VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', NOW(), '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow' );
UPSERT INTO RUOHBASE.SYS_CONFIG("config_id","config_name","config_key","config_value","config_type","create_by","create_time","update_by","update_time","remark")
VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', NOW(), '', NULL, '初始化密码 123456' );
UPSERT INTO RUOHBASE.SYS_CONFIG("config_id","config_name","config_key","config_value","config_type","create_by","create_time","update_by","update_time","remark")
VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', NOW(), '', NULL, '深色主题theme-dark，浅色主题theme-light' );
UPSERT INTO RUOHBASE.SYS_CONFIG("config_id","config_name","config_key","config_value","config_type","create_by","create_time","update_by","update_time","remark")
VALUES (4, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', NOW(), '', NULL, '是否开启注册用户功能（true开启，false关闭）');
UPSERT INTO RUOHBASE.SYS_CONFIG("config_id","config_name","config_key","config_value","config_type","create_by","create_time","update_by","update_time","remark")
VALUES (5, '用户登录-黑名单列表', 'sys.login.blackIPList', '', 'Y', 'admin', NOW(), '', NULL, '设置登录IP黑名单限制，多个匹配项以;分隔，支持匹配（*通配、网段）');



-- ----------------------------
-- 14、系统访问记录
-- ----------------------------
DROP TABLE IF EXISTS RUOHBASE.SYS_LOGININFOR;
CREATE TABLE RUOHBASE.SYS_LOGININFOR (
                                         "info_id"        INTEGER           NOT NULL  PRIMARY KEY,    -- 访问ID
                                         "user_name"      VARCHAR(50)       DEFAULT '',               -- 用户账号
                                         "ipaddr"         VARCHAR(128)      DEFAULT '',               -- 登录IP地址
                                         "status"         CHAR(1)           DEFAULT '0',              -- 登录状态（0成功 1失败）
                                         "msg"            VARCHAR(255)      DEFAULT '',               -- 提示信息
                                         "access_time"    DATE                                        -- 访问时间
);



DROP SEQUENCE IF EXISTS RUOHBASE.SYS_LOGININFOR_SEQUENCE;
CREATE SEQUENCE RUOHBASE.SYS_LOGININFOR_SEQUENCE START WITH 1 increment BY 1;
DROP INDEX IF EXISTS sys_logininfor_idx_1 ON RUOHBASE.SYS_LOGININFOR;
DROP INDEX IF EXISTS sys_logininfor_idx_2 ON RUOHBASE.SYS_LOGININFOR;
DROP INDEX IF EXISTS sys_logininfor_idx_3 ON RUOHBASE.SYS_LOGININFOR;
DROP INDEX IF EXISTS sys_logininfor_idx_4 ON RUOHBASE.SYS_LOGININFOR;

CREATE INDEX sys_logininfor_idx_1 ON RUOHBASE.SYS_LOGININFOR ("ipaddr");
CREATE INDEX sys_logininfor_idx_2 ON RUOHBASE.SYS_LOGININFOR ("status");
CREATE INDEX sys_logininfor_idx_3 ON RUOHBASE.SYS_LOGININFOR ("user_name");
CREATE INDEX sys_logininfor_idx_4 ON RUOHBASE.SYS_LOGININFOR ("access_time");




-- ----------------------------
-- 15、定时任务调度表
-- ----------------------------
DROP TABLE IF EXISTS RUOHBASE.SYS_JOB;
CREATE TABLE RUOHBASE.SYS_JOB (
                                  "job_id"            INTEGER           NOT NULL  PRIMARY KEY,    -- 任务ID
                                  "job_name"          VARCHAR(64)       DEFAULT '',                               -- 任务名称
                                  "job_group"         VARCHAR(64)       DEFAULT 'DEFAULT',                        -- 任务组名
                                  "invoke_target"     VARCHAR(500)      ,                                 -- 调用目标字符串
                                  "cron_expression"   VARCHAR(255)      DEFAULT '',                               -- cron执行表达式
                                  "misfire_policy"    VARCHAR(20)       DEFAULT '3',                              -- 计划执行错误策略（1立即执行 2执行一次 3放弃执行）
                                  "concurrent"        CHAR(1)           DEFAULT '1',                              -- 是否并发执行（0允许 1禁止）
                                  "status"            CHAR(1)           DEFAULT '0',                              -- 状态（0正常 1暂停）
                                  "create_by"         VARCHAR(64)       DEFAULT '',                               -- 创建者
                                  "create_time"       DATE,                                                      -- 创建时间
                                  "update_by"         VARCHAR(64)       DEFAULT '',                               -- 更新者
                                  "update_time"       DATE,                                                      -- 更新时间
                                  "remark"            VARCHAR(500)      DEFAULT ''                                -- 备注信息
);

DROP SEQUENCE IF EXISTS RUOHBASE.SYS_JOB_SEQUENCE;
CREATE SEQUENCE RUOHBASE.SYS_JOB_SEQUENCE START WITH 1 increment BY 1;
DROP INDEX IF EXISTS sys_job_idx_1 ON RUOHBASE.SYS_JOB;
DROP INDEX IF EXISTS sys_job_idx_2 ON RUOHBASE.SYS_JOB;
DROP INDEX IF EXISTS sys_job_idx_3 ON RUOHBASE.SYS_JOB;
DROP INDEX IF EXISTS sys_job_idx_4 ON RUOHBASE.SYS_JOB;

CREATE INDEX sys_job_idx_1 ON RUOHBASE.SYS_JOB ("job_name");
CREATE INDEX sys_job_idx_2 ON RUOHBASE.SYS_JOB ("job_group");
CREATE INDEX sys_job_idx_3 ON RUOHBASE.SYS_JOB ("status");
CREATE INDEX sys_job_idx_4 ON RUOHBASE.SYS_JOB ("invoke_target");


UPSERT INTO RUOHBASE.SYS_JOB("job_id","job_name","job_group","invoke_target","cron_expression","misfire_policy","concurrent","status","create_by","create_time","update_by","update_time","remark")
VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_JOB("job_id","job_name","job_group","invoke_target","cron_expression","misfire_policy","concurrent","status","create_by","create_time","update_by","update_time","remark")
VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', NOW(), '', NULL, '');
UPSERT INTO RUOHBASE.SYS_JOB("job_id","job_name","job_group","invoke_target","cron_expression","misfire_policy","concurrent","status","create_by","create_time","update_by","update_time","remark")
VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', NOW(), '', NULL, '');


-- ----------------------------
-- 16、定时任务调度日志表
-- ----------------------------
DROP TABLE IF EXISTS  RUOHBASE.SYS_JOB_LOG;
CREATE TABLE  RUOHBASE.SYS_JOB_LOG (
                                       "job_log_id"          INTEGER           NOT NULL  PRIMARY KEY,        -- 任务日志ID
                                       "job_name"            VARCHAR(64)       ,                     -- 任务名称
                                       "job_group"           VARCHAR(64)       ,                     -- 任务组名
                                       "invoke_target"       VARCHAR(500)      ,                     -- 调用目标字符串
                                       "job_message"         VARCHAR(500),                                   -- 日志信息
                                       "status"              CHAR(1)           DEFAULT '0',                  -- 执行状态（0正常 1失败）
                                       "exception_info"      VARCHAR(2000)     DEFAULT '',                   -- 异常信息
                                       "create_time"         DATE                                           -- 创建时间
);

DROP SEQUENCE IF EXISTS RUOHBASE.SYS_JOB_LOG_SEQUENCE;
CREATE SEQUENCE RUOHBASE.SYS_JOB_LOG_SEQUENCE START WITH 1 increment BY 1;
DROP INDEX IF EXISTS sys_job_log_idx_1 ON RUOHBASE.SYS_JOB_LOG;
DROP INDEX IF EXISTS sys_job_log_idx_2 ON RUOHBASE.SYS_JOB_LOG;
DROP INDEX IF EXISTS sys_job_log_idx_3 ON RUOHBASE.SYS_JOB_LOG;
DROP INDEX IF EXISTS sys_job_log_idx_4 ON RUOHBASE.SYS_JOB_LOG;
DROP INDEX IF EXISTS sys_job_log_idx_5 ON RUOHBASE.SYS_JOB_LOG;

CREATE INDEX sys_job_log_idx_1 ON RUOHBASE.SYS_JOB_LOG ("job_name");
CREATE INDEX sys_job_log_idx_2 ON RUOHBASE.SYS_JOB_LOG ("job_group");
CREATE INDEX sys_job_log_idx_3 ON RUOHBASE.SYS_JOB_LOG ("status");
CREATE INDEX sys_job_log_idx_4 ON RUOHBASE.SYS_JOB_LOG ("invoke_target");
CREATE INDEX sys_job_log_idx_5 ON RUOHBASE.SYS_JOB_LOG ("create_time");


-- ----------------------------
-- 17、通知公告表
-- ----------------------------
DROP TABLE IF EXISTS RUOHBASE.SYS_NOTICE;
CREATE TABLE RUOHBASE.SYS_NOTICE (
                                     "notice_id"         INTEGER           NOT NULL  PRIMARY KEY,  -- 公告ID
                                     "notice_title"      VARCHAR(50)       ,                            -- 公告标题
                                     "notice_type"       CHAR(1)           ,                            -- 公告类型（1通知 2公告）
                                     "notice_content"    VARCHAR(500)          DEFAULT NULL,                        -- 公告内容
                                     "status"            CHAR(1)           DEFAULT '0',                         -- 公告状态（0正常 1关闭）
                                     "create_by"         VARCHAR(64)       DEFAULT '',                          -- 创建者
                                     "create_time"       DATE,                                             -- 创建时间
                                     "update_by"         VARCHAR(64)       DEFAULT '',                          -- 更新者
                                     "update_time"       DATE,                                             -- 更新时间
                                     "remark"            VARCHAR(255)      DEFAULT NULL                         -- 备注
);


DROP SEQUENCE IF EXISTS RUOHBASE.SYS_NOTICE_SEQUENCE;
CREATE SEQUENCE RUOHBASE.SYS_NOTICE_SEQUENCE START WITH 1 increment BY 1;
DROP INDEX IF EXISTS sys_notice_idx_1 ON RUOHBASE.SYS_NOTICE;
DROP INDEX IF EXISTS sys_notice_idx_2 ON RUOHBASE.SYS_NOTICE;
DROP INDEX IF EXISTS sys_notice_idx_3 ON RUOHBASE.SYS_NOTICE;

CREATE INDEX sys_notice_idx_1 ON RUOHBASE.SYS_NOTICE ("notice_title");
CREATE INDEX sys_notice_idx_2 ON RUOHBASE.SYS_NOTICE ("notice_type");
CREATE INDEX sys_notice_idx_3 ON RUOHBASE.SYS_NOTICE ("create_by");

-- ----------------------------
-- 初始化-公告信息表数据
-- ----------------------------
UPSERT INTO RUOHBASE.SYS_NOTICE("notice_id","notice_title","notice_type","notice_content","status","create_by","create_time","update_by","update_time","remark")
VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', '新版本内容', '0', 'admin', NOW(), '', NULL, '管理员');
UPSERT INTO RUOHBASE.SYS_NOTICE("notice_id","notice_title","notice_type","notice_content","status","create_by","create_time","update_by","update_time","remark")
VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', '维护内容', '0', 'admin', NOW(), '', NULL, '管理员');


-- ----------------------------
-- 18、代码生成业务表
-- ----------------------------
DROP TABLE IF EXISTS RUOHBASE.GEN_TABLE;
CREATE TABLE RUOHBASE.GEN_TABLE (
                                    "table_id"          INTEGER           NOT NULL  PRIMARY KEY,        -- 编号
                                    "table_name"        VARCHAR(200)      DEFAULT '',                   -- 表名称
                                    "table_comment"     VARCHAR(500)      DEFAULT '',                   -- 表描述
                                    "sub_table_name"    VARCHAR(64)       DEFAULT NULL,                 -- 关联子表的表名
                                    "sub_table_fk_name" VARCHAR(64)       DEFAULT NULL,                 -- 子表关联的外键名
                                    "class_name"        VARCHAR(100)      DEFAULT '',                   -- 实体类名称
                                    "tpl_category"      VARCHAR(200)      DEFAULT 'crud',              -- 使用的模板（crud单表操作 tree树表操作）
                                    "package_name"      VARCHAR(100),                                  -- 生成包路径
                                    "module_name"       VARCHAR(30),                                   -- 生成模块名
                                    "business_name"     VARCHAR(30),                                   -- 生成业务名
                                    "function_name"     VARCHAR(50),                                   -- 生成功能名
                                    "function_author"   VARCHAR(50),                                   -- 生成功能作者
                                    "gen_type"          CHAR(1)           DEFAULT '0',                  -- 生成代码方式（0zip压缩包 1自定义路径）
                                    "gen_path"          VARCHAR(200)      DEFAULT '/',                  -- 生成路径（不填默认项目路径）
                                    "options"           VARCHAR(1000),                                 -- 其它生成选项
                                    "create_by"         VARCHAR(64)       DEFAULT '',                   -- 创建者
                                    "create_time"       DATE,                                          -- 创建时间
                                    "update_by"         VARCHAR(64)       DEFAULT '',                   -- 更新者
                                    "update_time"       DATE,                                          -- 更新时间
                                    "remark"            VARCHAR(500)      DEFAULT NULL                  -- 备注
);


DROP SEQUENCE IF EXISTS RUOHBASE.GEN_TABLE_SEQUENCE;
CREATE SEQUENCE RUOHBASE.GEN_TABLE_SEQUENCE START WITH 1 increment BY 1;
DROP INDEX IF EXISTS gen_table_idx_1 ON RUOHBASE.GEN_TABLE;
DROP INDEX IF EXISTS gen_table_idx_2 ON RUOHBASE.GEN_TABLE;
DROP INDEX IF EXISTS gen_table_idx_3 ON RUOHBASE.GEN_TABLE;

CREATE INDEX gen_table_idx_1 ON RUOHBASE.GEN_TABLE ("table_name");
CREATE INDEX gen_table_idx_2 ON RUOHBASE.GEN_TABLE ("table_comment");
CREATE INDEX gen_table_idx_3 ON RUOHBASE.GEN_TABLE ("create_time");


-- ----------------------------
-- 19、代码生成业务表字段
-- ----------------------------
DROP TABLE IF EXISTS RUOHBASE.GEN_TABLE_COLUMN;
CREATE TABLE RUOHBASE.GEN_TABLE_COLUMN (
                                           "column_id"         INTEGER           NOT NULL  PRIMARY KEY,        -- 编号
                                           "table_id"          INTEGER,                                        -- 归属表编号
                                           "column_name"       VARCHAR(200),                                   -- 列名称
                                           "column_comment"    VARCHAR(500),                                   -- 列描述
                                           "column_type"       VARCHAR(100),                                   -- 列类型
                                           "java_type"         VARCHAR(500),                                   -- JAVA类型
                                           "java_field"        VARCHAR(200),                                   -- JAVA字段名
                                           "is_pk"             CHAR(1),                                        -- 是否主键（1是）
                                           "is_increment"      CHAR(1),                                        -- 是否自增（1是）
                                           "is_required"       CHAR(1),                                        -- 是否必填（1是）
                                           "is_insert"         CHAR(1),                                        -- 是否为插入字段（1是）
                                           "is_edit"           CHAR(1),                                        -- 是否编辑字段（1是）
                                           "is_list"           CHAR(1),                                        -- 是否列表字段（1是）
                                           "is_query"          CHAR(1),                                        -- 是否查询字段（1是）
                                           "query_type"        VARCHAR(200)      DEFAULT 'EQ',                 -- 查询方式（等于、不等于、大于、小于、范围）
                                           "html_type"         VARCHAR(200),                                   -- 显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）
                                           "dict_type"         VARCHAR(200)      DEFAULT '',                   -- 字典类型
                                           "sort"              INTEGER,                                        -- 排序
                                           "create_by"         VARCHAR(64)       DEFAULT '',                   -- 创建者
                                           "create_time"       DATE,                                           -- 创建时间
                                           "update_by"         VARCHAR(64)       DEFAULT '',                   -- 更新者
                                           "update_time"       DATE,                                           -- 更新时间
                                           "remark"            VARCHAR(500)      DEFAULT ''                    -- 备注
);

DROP SEQUENCE IF EXISTS RUOHBASE.GEN_TABLE_COLUMN_SEQUENCE;
CREATE SEQUENCE RUOHBASE.GEN_TABLE_COLUMN_SEQUENCE START WITH 1 increment BY 1;
