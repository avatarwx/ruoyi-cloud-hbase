package com.ruoyi.jobcust;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RuoyiJobCustApplication {

    public static void main(String[] args) {
        SpringApplication.run(RuoyiJobCustApplication.class, args);
    }

}
